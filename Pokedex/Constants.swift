//
//  Constants.swift
//  Pokedex
//
//  Created by Noel Hwande on 1/26/16.
//  Copyright © 2016 Nodza. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()